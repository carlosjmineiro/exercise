package net.agroop.domain.user;

import net.agroop.domain.api.ApiKey;

import javax.persistence.*;
import java.io.Serializable;

/**
 * User.java
 * Created by Rúben Madeira on 05/01/2018 - 10:40.
 * Copyright 2018 © eAgroop,Lda
 */
@Entity
public class User implements Serializable{

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String name;

    private String email;

    private String password;

    @OneToOne(mappedBy = "user")
    private ApiKey apiKey;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ApiKey getApiKey() {
        return apiKey;
    }

    public void setApiKey(ApiKey apiKey) {
        this.apiKey = apiKey;
    }
}

