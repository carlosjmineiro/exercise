package net.agroop.serviceimpl.api;

import net.agroop.domain.api.ApiKey;
import net.agroop.repository.api.ApiKeyRepository;
import net.agroop.service.api.ApiKeyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * ApiServiceImpl.java
 * Created by Rúben Madeira on 05/01/2018 - 11:07.
 * Copyright 2018 © eAgroop,Lda
 */
@Service
public class ApiServiceImpl implements ApiKeyService {

    @Autowired
    ApiKeyRepository apiKeyRepository;

    @Override
    @Transactional
    public ApiKey save(ApiKey apiKey) {
        return apiKeyRepository.save(apiKey);
    }
}
