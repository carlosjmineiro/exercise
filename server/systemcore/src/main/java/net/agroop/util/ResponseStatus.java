package net.agroop.util;

/**
 * ResponseStatus.java
 * Created by Rúben Madeira on 05/01/2018 - 22:09.
 * Copyright 2018 © eAgroop,Lda
 */
public class ResponseStatus {
    private String reason;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public static ResponseStatus withReason(String reason) {
        ResponseStatus status = new ResponseStatus();
        status.setReason(reason);
        return status;
    }

    public static ResponseStatus withReasonAndErrorCode(String reason) {
        ResponseStatus status = new ResponseStatus();
        status.setReason(reason);
        return status;
    }
}
