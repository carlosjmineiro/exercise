package net.agroop.controller;

import net.agroop.common.exception.UserEmailExistException;
import net.agroop.controller.endpoint.Endpoint;
import net.agroop.domain.user.User;
import net.agroop.request.RegisterRequest;
import net.agroop.response.RegisterResponse;
import net.agroop.service.user.UserService;
import net.agroop.util.ResponseEntityHelper;
import net.agroop.util.ResponseStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.Callable;

/**
 * RegisterServiceController.java
 * Created by Rúben Madeira on 05/01/2018 - 21:52.
 * Copyright 2018 © eAgroop,Lda
 */
@RestController
@RequestMapping(Endpoint.REGISTRATION)
public class RegisterServiceController {

    private static final Logger logger = LoggerFactory.getLogger(RegisterServiceController.class);

    @Autowired
    UserService userService;

    @RequestMapping(method = RequestMethod.POST)
    public Callable<ResponseEntity> register(@RequestBody RegisterRequest request) {
        return () -> {
            try {
                User user = userService.createUser(request.getName(), request.getEmail(), request.getPassword());
                RegisterResponse response = new RegisterResponse(user.getApiKey().getId());
                return ResponseEntityHelper.ok(response);
            } catch (UserEmailExistException e) {
                return ResponseEntityHelper.badRequest(ResponseStatus.withReason(e.getMessage()));
            } catch (Exception e) {
                logger.error("500 SERVER ERROR", e);
                return ResponseEntityHelper.serverError();
            }
        };
    }

}
