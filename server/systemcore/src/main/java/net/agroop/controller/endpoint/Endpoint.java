package net.agroop.controller.endpoint;

/**
 * Endpoint.java
 * Created by Rúben Madeira on 05/01/2018 - 21:50.
 * Copyright 2018 © eAgroop,Lda
 */
public class Endpoint {

    /** Current API Version **/
    private static final String currentAPIVersion = "/v1";

    /** Registration **/
    public static final String REGISTRATION = currentAPIVersion + "/registration";
}
