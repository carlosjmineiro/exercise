Multimodule Maven Project

-platform

--------common      - common files / configurations

--------domain      - domain classes

--------repository  - spring data repository interfaces

--------service     - services interfaces 

--------serviceimpl - services implementation    

--------systemcore    Spring boot App - consumes all other modules

---pom - multimodule general pom


SystemCore Module exposes a REST API in net.agroop.controller.RegisterServiceController and using JDBC and MySQL as Database, check application.properties
    
RUN Platform - Spring boot APP
    
    Go to Run/Debug
        Add Spring boot
            Define Name Platform App
                Main Class: net.agroop.SystemCoreApplication
                    Use Classpath of Module : systemcore
                        Apply 
                            OK
    
    Click on Debug Platform App
    
    Platform App in runing in http://localhost:8090/
    
    Testing in Postman:
        POST http://localhost:8090/v1/registration HTTP/1.1
        Host: localhost:8090
        Content-Type: application/json
        Cache-Control: no-cache
            {
	            "name":"ruben",
	            "email":"rubenmadeira@gmail.com",
	            "password":"pass"
            }
            
        On sucess:
            200 OK Check on MySQL in table user and api_key, to see result
             
        On Failure:
            400 Bad Request
            
            {
                "reason": "There is an user with that email: rubenmadeira@gmail.com"
            }
            
            Or
            
            500 Internal Server Error
                
                
                
                            